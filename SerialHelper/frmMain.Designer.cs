﻿namespace SerialHelper
{
	partial class frmMain
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			this.status_bottom = new System.Windows.Forms.StatusStrip();
			this.slab_info = new System.Windows.Forms.ToolStripStatusLabel();
			this.slab_send = new System.Windows.Forms.ToolStripStatusLabel();
			this.slab_recv = new System.Windows.Forms.ToolStripStatusLabel();
			this.menu_show_recv = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.CopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.PasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.SelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.ClearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.spc_back = new System.Windows.Forms.SplitContainer();
			this.grp_send_config = new System.Windows.Forms.GroupBox();
			this.chk_send_lua = new System.Windows.Forms.CheckBox();
			this.chk_send_hex = new System.Windows.Forms.CheckBox();
			this.chk_send_clear = new System.Windows.Forms.CheckBox();
			this.chk_send_file = new System.Windows.Forms.CheckBox();
			this.lbl_send_clear = new System.Windows.Forms.Label();
			this.lbl_send_load = new System.Windows.Forms.Label();
			this.grp_recv_config = new System.Windows.Forms.GroupBox();
			this.chk_send_show = new System.Windows.Forms.CheckBox();
			this.chk_recv_time = new System.Windows.Forms.CheckBox();
			this.chk_recv_file = new System.Windows.Forms.CheckBox();
			this.lbl_recv_clear = new System.Windows.Forms.Label();
			this.lbl_recv_save = new System.Windows.Forms.Label();
			this.chk_recv_show = new System.Windows.Forms.CheckBox();
			this.chk_recv_hex = new System.Windows.Forms.CheckBox();
			this.grp_port_config = new System.Windows.Forms.GroupBox();
			this.btn_port_open = new System.Windows.Forms.Button();
			this.cbo_port_data_bits = new System.Windows.Forms.ComboBox();
			this.cbo_port_stop_bits = new System.Windows.Forms.ComboBox();
			this.cbo_port_parity = new System.Windows.Forms.ComboBox();
			this.cbo_port_baud_rate = new System.Windows.Forms.ComboBox();
			this.cbo_port_name = new System.Windows.Forms.ComboBox();
			this.lbl_port_data_bits = new System.Windows.Forms.Label();
			this.lbl_port_stop_bits = new System.Windows.Forms.Label();
			this.lbl_port_parity = new System.Windows.Forms.Label();
			this.lbl_port_baud_rate = new System.Windows.Forms.Label();
			this.lbl_port_name = new System.Windows.Forms.Label();
			this.spc_show = new System.Windows.Forms.SplitContainer();
			this.grp_show_recv = new System.Windows.Forms.GroupBox();
			this.lbl_hide_down = new System.Windows.Forms.Label();
			this.lbl_hide_left = new System.Windows.Forms.Label();
			this.txt_show_recv = new System.Windows.Forms.TextBox();
			this.grp_show_send = new System.Windows.Forms.GroupBox();
			this.spc_pack = new System.Windows.Forms.SplitContainer();
			this.txt_pack_end = new System.Windows.Forms.TextBox();
			this.txt_pack_head = new System.Windows.Forms.TextBox();
			this.chk_pack_end = new System.Windows.Forms.CheckBox();
			this.chk_pack_head = new System.Windows.Forms.CheckBox();
			this.cbo_pack_check = new System.Windows.Forms.ComboBox();
			this.lbl_pack_interval_ms = new System.Windows.Forms.Label();
			this.chk_pack_check = new System.Windows.Forms.CheckBox();
			this.num_pack_loop = new System.Windows.Forms.NumericUpDown();
			this.chk_pack_loop = new System.Windows.Forms.CheckBox();
			this.txt_show_send = new System.Windows.Forms.TextBox();
			this.btn_show_send = new System.Windows.Forms.Button();
			this.status_bottom.SuspendLayout();
			this.menu_show_recv.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spc_back)).BeginInit();
			this.spc_back.Panel1.SuspendLayout();
			this.spc_back.Panel2.SuspendLayout();
			this.spc_back.SuspendLayout();
			this.grp_send_config.SuspendLayout();
			this.grp_recv_config.SuspendLayout();
			this.grp_port_config.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spc_show)).BeginInit();
			this.spc_show.Panel1.SuspendLayout();
			this.spc_show.Panel2.SuspendLayout();
			this.spc_show.SuspendLayout();
			this.grp_show_recv.SuspendLayout();
			this.grp_show_send.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spc_pack)).BeginInit();
			this.spc_pack.Panel1.SuspendLayout();
			this.spc_pack.Panel2.SuspendLayout();
			this.spc_pack.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.num_pack_loop)).BeginInit();
			this.SuspendLayout();
			// 
			// status_bottom
			// 
			this.status_bottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.slab_info,
            this.slab_send,
            this.slab_recv});
			this.status_bottom.Location = new System.Drawing.Point(0, 537);
			this.status_bottom.Name = "status_bottom";
			this.status_bottom.Size = new System.Drawing.Size(619, 22);
			this.status_bottom.TabIndex = 3;
			this.status_bottom.Text = "statusStrip1";
			// 
			// slab_info
			// 
			this.slab_info.IsLink = true;
			this.slab_info.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
			this.slab_info.LinkColor = System.Drawing.SystemColors.ControlText;
			this.slab_info.Name = "slab_info";
			this.slab_info.Size = new System.Drawing.Size(342, 17);
			this.slab_info.Spring = true;
			this.slab_info.Text = "  成都迈硕电气有限公司 www.mysoow.com";
			this.slab_info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.slab_info.Click += new System.EventHandler(this.slab_info_Click);
			// 
			// slab_send
			// 
			this.slab_send.AutoSize = false;
			this.slab_send.Name = "slab_send";
			this.slab_send.Size = new System.Drawing.Size(131, 17);
			this.slab_send.Text = "发送:0";
			this.slab_send.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// slab_recv
			// 
			this.slab_recv.AutoSize = false;
			this.slab_recv.Name = "slab_recv";
			this.slab_recv.Size = new System.Drawing.Size(131, 17);
			this.slab_recv.Text = "接收:0";
			this.slab_recv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// menu_show_recv
			// 
			this.menu_show_recv.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyToolStripMenuItem,
            this.PasteToolStripMenuItem,
            this.toolStripMenuItem1,
            this.SelectAllToolStripMenuItem,
            this.toolStripMenuItem2,
            this.ClearToolStripMenuItem});
			this.menu_show_recv.Name = "menu_show_recv";
			this.menu_show_recv.Size = new System.Drawing.Size(140, 104);
			this.menu_show_recv.Opened += new System.EventHandler(this.menu_show_recv_Opened);
			// 
			// CopyToolStripMenuItem
			// 
			this.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem";
			this.CopyToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
			this.CopyToolStripMenuItem.Text = "复制(&C)";
			this.CopyToolStripMenuItem.Click += new System.EventHandler(this.CopyToolStripMenuItem_Click);
			// 
			// PasteToolStripMenuItem
			// 
			this.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem";
			this.PasteToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
			this.PasteToolStripMenuItem.Text = "粘贴(&P)";
			this.PasteToolStripMenuItem.Click += new System.EventHandler(this.PasteToolStripMenuItem_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(136, 6);
			// 
			// SelectAllToolStripMenuItem
			// 
			this.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem";
			this.SelectAllToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
			this.SelectAllToolStripMenuItem.Text = "全选(&A)";
			this.SelectAllToolStripMenuItem.Click += new System.EventHandler(this.SelectAllToolStripMenuItem_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(136, 6);
			// 
			// ClearToolStripMenuItem
			// 
			this.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem";
			this.ClearToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
			this.ClearToolStripMenuItem.Text = "清除显示(&S)";
			this.ClearToolStripMenuItem.Click += new System.EventHandler(this.ClearToolStripMenuItem_Click);
			// 
			// spc_back
			// 
			this.spc_back.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.spc_back.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.spc_back.IsSplitterFixed = true;
			this.spc_back.Location = new System.Drawing.Point(12, 12);
			this.spc_back.Name = "spc_back";
			// 
			// spc_back.Panel1
			// 
			this.spc_back.Panel1.Controls.Add(this.grp_send_config);
			this.spc_back.Panel1.Controls.Add(this.grp_recv_config);
			this.spc_back.Panel1.Controls.Add(this.grp_port_config);
			// 
			// spc_back.Panel2
			// 
			this.spc_back.Panel2.Controls.Add(this.spc_show);
			this.spc_back.Size = new System.Drawing.Size(595, 514);
			this.spc_back.SplitterDistance = 143;
			this.spc_back.TabIndex = 5;
			// 
			// grp_send_config
			// 
			this.grp_send_config.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.grp_send_config.Controls.Add(this.chk_send_lua);
			this.grp_send_config.Controls.Add(this.chk_send_hex);
			this.grp_send_config.Controls.Add(this.chk_send_clear);
			this.grp_send_config.Controls.Add(this.chk_send_file);
			this.grp_send_config.Controls.Add(this.lbl_send_clear);
			this.grp_send_config.Controls.Add(this.lbl_send_load);
			this.grp_send_config.Location = new System.Drawing.Point(0, 375);
			this.grp_send_config.Name = "grp_send_config";
			this.grp_send_config.Size = new System.Drawing.Size(143, 139);
			this.grp_send_config.TabIndex = 5;
			this.grp_send_config.TabStop = false;
			this.grp_send_config.Text = "发送设置";
			// 
			// chk_send_lua
			// 
			this.chk_send_lua.AutoSize = true;
			this.chk_send_lua.Location = new System.Drawing.Point(14, 87);
			this.chk_send_lua.Name = "chk_send_lua";
			this.chk_send_lua.Size = new System.Drawing.Size(96, 16);
			this.chk_send_lua.TabIndex = 6;
			this.chk_send_lua.Text = "发送执行脚本";
			this.chk_send_lua.UseVisualStyleBackColor = true;
			// 
			// chk_send_hex
			// 
			this.chk_send_hex.AutoSize = true;
			this.chk_send_hex.Location = new System.Drawing.Point(14, 65);
			this.chk_send_hex.Name = "chk_send_hex";
			this.chk_send_hex.Size = new System.Drawing.Size(96, 16);
			this.chk_send_hex.TabIndex = 2;
			this.chk_send_hex.Text = "发送十六进制";
			this.chk_send_hex.UseVisualStyleBackColor = true;
			this.chk_send_hex.CheckedChanged += new System.EventHandler(this.chk_send_hex_CheckedChanged);
			// 
			// chk_send_clear
			// 
			this.chk_send_clear.AutoSize = true;
			this.chk_send_clear.Location = new System.Drawing.Point(14, 43);
			this.chk_send_clear.Name = "chk_send_clear";
			this.chk_send_clear.Size = new System.Drawing.Size(96, 16);
			this.chk_send_clear.TabIndex = 1;
			this.chk_send_clear.Text = "发送完成清空";
			this.chk_send_clear.UseVisualStyleBackColor = true;
			// 
			// chk_send_file
			// 
			this.chk_send_file.AutoSize = true;
			this.chk_send_file.Location = new System.Drawing.Point(14, 21);
			this.chk_send_file.Name = "chk_send_file";
			this.chk_send_file.Size = new System.Drawing.Size(114, 16);
			this.chk_send_file.TabIndex = 0;
			this.chk_send_file.Text = "发送文件数据...";
			this.chk_send_file.UseVisualStyleBackColor = true;
			this.chk_send_file.CheckedChanged += new System.EventHandler(this.chk_send_file_CheckedChanged);
			// 
			// lbl_send_clear
			// 
			this.lbl_send_clear.AutoSize = true;
			this.lbl_send_clear.Cursor = System.Windows.Forms.Cursors.Hand;
			this.lbl_send_clear.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.lbl_send_clear.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.lbl_send_clear.Location = new System.Drawing.Point(77, 116);
			this.lbl_send_clear.Name = "lbl_send_clear";
			this.lbl_send_clear.Size = new System.Drawing.Size(53, 12);
			this.lbl_send_clear.TabIndex = 5;
			this.lbl_send_clear.Text = "清除输入";
			this.lbl_send_clear.Click += new System.EventHandler(this.lbl_send_clear_Click);
			// 
			// lbl_send_load
			// 
			this.lbl_send_load.AutoSize = true;
			this.lbl_send_load.Cursor = System.Windows.Forms.Cursors.Hand;
			this.lbl_send_load.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.lbl_send_load.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.lbl_send_load.Location = new System.Drawing.Point(12, 116);
			this.lbl_send_load.Name = "lbl_send_load";
			this.lbl_send_load.Size = new System.Drawing.Size(53, 12);
			this.lbl_send_load.TabIndex = 4;
			this.lbl_send_load.Text = "载入数据";
			this.lbl_send_load.Click += new System.EventHandler(this.lbl_send_load_Click);
			// 
			// grp_recv_config
			// 
			this.grp_recv_config.Controls.Add(this.chk_send_show);
			this.grp_recv_config.Controls.Add(this.chk_recv_time);
			this.grp_recv_config.Controls.Add(this.chk_recv_file);
			this.grp_recv_config.Controls.Add(this.lbl_recv_clear);
			this.grp_recv_config.Controls.Add(this.lbl_recv_save);
			this.grp_recv_config.Controls.Add(this.chk_recv_show);
			this.grp_recv_config.Controls.Add(this.chk_recv_hex);
			this.grp_recv_config.Location = new System.Drawing.Point(0, 209);
			this.grp_recv_config.Name = "grp_recv_config";
			this.grp_recv_config.Size = new System.Drawing.Size(143, 160);
			this.grp_recv_config.TabIndex = 4;
			this.grp_recv_config.TabStop = false;
			this.grp_recv_config.Text = "接收设置";
			// 
			// chk_send_show
			// 
			this.chk_send_show.AutoSize = true;
			this.chk_send_show.Location = new System.Drawing.Point(14, 86);
			this.chk_send_show.Name = "chk_send_show";
			this.chk_send_show.Size = new System.Drawing.Size(96, 16);
			this.chk_send_show.TabIndex = 3;
			this.chk_send_show.Text = "显示发送数据";
			this.chk_send_show.UseVisualStyleBackColor = true;
			// 
			// chk_recv_time
			// 
			this.chk_recv_time.AutoSize = true;
			this.chk_recv_time.Location = new System.Drawing.Point(14, 42);
			this.chk_recv_time.Name = "chk_recv_time";
			this.chk_recv_time.Size = new System.Drawing.Size(96, 16);
			this.chk_recv_time.TabIndex = 1;
			this.chk_recv_time.Text = "显示接收时间";
			this.chk_recv_time.UseVisualStyleBackColor = true;
			// 
			// chk_recv_file
			// 
			this.chk_recv_file.AutoSize = true;
			this.chk_recv_file.Location = new System.Drawing.Point(14, 20);
			this.chk_recv_file.Name = "chk_recv_file";
			this.chk_recv_file.Size = new System.Drawing.Size(114, 16);
			this.chk_recv_file.TabIndex = 0;
			this.chk_recv_file.Text = "接收转向文件...";
			this.chk_recv_file.UseVisualStyleBackColor = true;
			this.chk_recv_file.CheckedChanged += new System.EventHandler(this.chk_recv_file_CheckedChanged);
			// 
			// lbl_recv_clear
			// 
			this.lbl_recv_clear.AutoSize = true;
			this.lbl_recv_clear.Cursor = System.Windows.Forms.Cursors.Hand;
			this.lbl_recv_clear.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.lbl_recv_clear.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.lbl_recv_clear.Location = new System.Drawing.Point(75, 137);
			this.lbl_recv_clear.Name = "lbl_recv_clear";
			this.lbl_recv_clear.Size = new System.Drawing.Size(53, 12);
			this.lbl_recv_clear.TabIndex = 6;
			this.lbl_recv_clear.Text = "清除显示";
			this.lbl_recv_clear.Click += new System.EventHandler(this.lbl_recv_clear_Click);
			// 
			// lbl_recv_save
			// 
			this.lbl_recv_save.AutoSize = true;
			this.lbl_recv_save.Cursor = System.Windows.Forms.Cursors.Hand;
			this.lbl_recv_save.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.lbl_recv_save.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.lbl_recv_save.Location = new System.Drawing.Point(10, 137);
			this.lbl_recv_save.Name = "lbl_recv_save";
			this.lbl_recv_save.Size = new System.Drawing.Size(53, 12);
			this.lbl_recv_save.TabIndex = 5;
			this.lbl_recv_save.Text = "保存数据";
			this.lbl_recv_save.Click += new System.EventHandler(this.lbl_recv_save_Click);
			// 
			// chk_recv_show
			// 
			this.chk_recv_show.AutoSize = true;
			this.chk_recv_show.Location = new System.Drawing.Point(14, 108);
			this.chk_recv_show.Name = "chk_recv_show";
			this.chk_recv_show.Size = new System.Drawing.Size(96, 16);
			this.chk_recv_show.TabIndex = 4;
			this.chk_recv_show.Text = "显示接收数据";
			this.chk_recv_show.UseVisualStyleBackColor = true;
			// 
			// chk_recv_hex
			// 
			this.chk_recv_hex.AutoSize = true;
			this.chk_recv_hex.Location = new System.Drawing.Point(14, 64);
			this.chk_recv_hex.Name = "chk_recv_hex";
			this.chk_recv_hex.Size = new System.Drawing.Size(96, 16);
			this.chk_recv_hex.TabIndex = 2;
			this.chk_recv_hex.Text = "显示十六进制";
			this.chk_recv_hex.UseVisualStyleBackColor = true;
			// 
			// grp_port_config
			// 
			this.grp_port_config.Controls.Add(this.btn_port_open);
			this.grp_port_config.Controls.Add(this.cbo_port_data_bits);
			this.grp_port_config.Controls.Add(this.cbo_port_stop_bits);
			this.grp_port_config.Controls.Add(this.cbo_port_parity);
			this.grp_port_config.Controls.Add(this.cbo_port_baud_rate);
			this.grp_port_config.Controls.Add(this.cbo_port_name);
			this.grp_port_config.Controls.Add(this.lbl_port_data_bits);
			this.grp_port_config.Controls.Add(this.lbl_port_stop_bits);
			this.grp_port_config.Controls.Add(this.lbl_port_parity);
			this.grp_port_config.Controls.Add(this.lbl_port_baud_rate);
			this.grp_port_config.Controls.Add(this.lbl_port_name);
			this.grp_port_config.Location = new System.Drawing.Point(0, 0);
			this.grp_port_config.Name = "grp_port_config";
			this.grp_port_config.Size = new System.Drawing.Size(143, 203);
			this.grp_port_config.TabIndex = 3;
			this.grp_port_config.TabStop = false;
			this.grp_port_config.Text = "串口设置";
			// 
			// btn_port_open
			// 
			this.btn_port_open.Image = ((System.Drawing.Image)(resources.GetObject("btn_port_open.Image")));
			this.btn_port_open.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_port_open.Location = new System.Drawing.Point(14, 153);
			this.btn_port_open.Name = "btn_port_open";
			this.btn_port_open.Size = new System.Drawing.Size(116, 37);
			this.btn_port_open.TabIndex = 10;
			this.btn_port_open.Text = "     打开串口";
			this.btn_port_open.UseVisualStyleBackColor = true;
			this.btn_port_open.Click += new System.EventHandler(this.btn_port_open_Click);
			// 
			// cbo_port_data_bits
			// 
			this.cbo_port_data_bits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbo_port_data_bits.FormattingEnabled = true;
			this.cbo_port_data_bits.Location = new System.Drawing.Point(65, 98);
			this.cbo_port_data_bits.Name = "cbo_port_data_bits";
			this.cbo_port_data_bits.Size = new System.Drawing.Size(65, 20);
			this.cbo_port_data_bits.TabIndex = 7;
			// 
			// cbo_port_stop_bits
			// 
			this.cbo_port_stop_bits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbo_port_stop_bits.FormattingEnabled = true;
			this.cbo_port_stop_bits.Location = new System.Drawing.Point(65, 124);
			this.cbo_port_stop_bits.Name = "cbo_port_stop_bits";
			this.cbo_port_stop_bits.Size = new System.Drawing.Size(65, 20);
			this.cbo_port_stop_bits.TabIndex = 9;
			// 
			// cbo_port_parity
			// 
			this.cbo_port_parity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbo_port_parity.FormattingEnabled = true;
			this.cbo_port_parity.Location = new System.Drawing.Point(65, 72);
			this.cbo_port_parity.Name = "cbo_port_parity";
			this.cbo_port_parity.Size = new System.Drawing.Size(65, 20);
			this.cbo_port_parity.TabIndex = 5;
			// 
			// cbo_port_baud_rate
			// 
			this.cbo_port_baud_rate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbo_port_baud_rate.FormattingEnabled = true;
			this.cbo_port_baud_rate.Location = new System.Drawing.Point(65, 46);
			this.cbo_port_baud_rate.Name = "cbo_port_baud_rate";
			this.cbo_port_baud_rate.Size = new System.Drawing.Size(65, 20);
			this.cbo_port_baud_rate.TabIndex = 3;
			// 
			// cbo_port_name
			// 
			this.cbo_port_name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbo_port_name.FormattingEnabled = true;
			this.cbo_port_name.Location = new System.Drawing.Point(65, 20);
			this.cbo_port_name.Name = "cbo_port_name";
			this.cbo_port_name.Size = new System.Drawing.Size(65, 20);
			this.cbo_port_name.TabIndex = 1;
			this.cbo_port_name.DropDown += new System.EventHandler(this.cbo_port_name_DropDown);
			// 
			// lbl_port_data_bits
			// 
			this.lbl_port_data_bits.AutoSize = true;
			this.lbl_port_data_bits.Location = new System.Drawing.Point(12, 101);
			this.lbl_port_data_bits.Name = "lbl_port_data_bits";
			this.lbl_port_data_bits.Size = new System.Drawing.Size(47, 12);
			this.lbl_port_data_bits.TabIndex = 6;
			this.lbl_port_data_bits.Text = "数据位:";
			// 
			// lbl_port_stop_bits
			// 
			this.lbl_port_stop_bits.AutoSize = true;
			this.lbl_port_stop_bits.Location = new System.Drawing.Point(12, 127);
			this.lbl_port_stop_bits.Name = "lbl_port_stop_bits";
			this.lbl_port_stop_bits.Size = new System.Drawing.Size(47, 12);
			this.lbl_port_stop_bits.TabIndex = 8;
			this.lbl_port_stop_bits.Text = "停止位:";
			// 
			// lbl_port_parity
			// 
			this.lbl_port_parity.AutoSize = true;
			this.lbl_port_parity.Location = new System.Drawing.Point(12, 75);
			this.lbl_port_parity.Name = "lbl_port_parity";
			this.lbl_port_parity.Size = new System.Drawing.Size(47, 12);
			this.lbl_port_parity.TabIndex = 4;
			this.lbl_port_parity.Text = "校验位:";
			// 
			// lbl_port_baud_rate
			// 
			this.lbl_port_baud_rate.AutoSize = true;
			this.lbl_port_baud_rate.Location = new System.Drawing.Point(12, 49);
			this.lbl_port_baud_rate.Name = "lbl_port_baud_rate";
			this.lbl_port_baud_rate.Size = new System.Drawing.Size(47, 12);
			this.lbl_port_baud_rate.TabIndex = 2;
			this.lbl_port_baud_rate.Text = "波特率:";
			// 
			// lbl_port_name
			// 
			this.lbl_port_name.AutoSize = true;
			this.lbl_port_name.Location = new System.Drawing.Point(12, 23);
			this.lbl_port_name.Name = "lbl_port_name";
			this.lbl_port_name.Size = new System.Drawing.Size(47, 12);
			this.lbl_port_name.TabIndex = 0;
			this.lbl_port_name.Text = "串口号:";
			// 
			// spc_show
			// 
			this.spc_show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.spc_show.Cursor = System.Windows.Forms.Cursors.Default;
			this.spc_show.Location = new System.Drawing.Point(0, 0);
			this.spc_show.Name = "spc_show";
			this.spc_show.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// spc_show.Panel1
			// 
			this.spc_show.Panel1.Controls.Add(this.grp_show_recv);
			this.spc_show.Panel1MinSize = 113;
			// 
			// spc_show.Panel2
			// 
			this.spc_show.Panel2.Controls.Add(this.grp_show_send);
			this.spc_show.Panel2MinSize = 113;
			this.spc_show.Size = new System.Drawing.Size(448, 514);
			this.spc_show.SplitterDistance = 397;
			this.spc_show.TabIndex = 5;
			// 
			// grp_show_recv
			// 
			this.grp_show_recv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.grp_show_recv.Controls.Add(this.lbl_hide_down);
			this.grp_show_recv.Controls.Add(this.lbl_hide_left);
			this.grp_show_recv.Controls.Add(this.txt_show_recv);
			this.grp_show_recv.Location = new System.Drawing.Point(0, 0);
			this.grp_show_recv.Name = "grp_show_recv";
			this.grp_show_recv.Size = new System.Drawing.Size(448, 397);
			this.grp_show_recv.TabIndex = 0;
			this.grp_show_recv.TabStop = false;
			this.grp_show_recv.Text = "接收数据";
			// 
			// lbl_hide_down
			// 
			this.lbl_hide_down.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_hide_down.Cursor = System.Windows.Forms.Cursors.Hand;
			this.lbl_hide_down.Location = new System.Drawing.Point(11, 387);
			this.lbl_hide_down.Name = "lbl_hide_down";
			this.lbl_hide_down.Size = new System.Drawing.Size(425, 8);
			this.lbl_hide_down.TabIndex = 2;
			this.lbl_hide_down.Click += new System.EventHandler(this.lbl_hide_down_Click);
			this.lbl_hide_down.MouseEnter += new System.EventHandler(this.lbl_hide_down_MouseEnter);
			this.lbl_hide_down.MouseLeave += new System.EventHandler(this.lbl_hide_down_MouseLeave);
			// 
			// lbl_hide_left
			// 
			this.lbl_hide_left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.lbl_hide_left.Cursor = System.Windows.Forms.Cursors.Hand;
			this.lbl_hide_left.Location = new System.Drawing.Point(2, 20);
			this.lbl_hide_left.Name = "lbl_hide_left";
			this.lbl_hide_left.Size = new System.Drawing.Size(8, 366);
			this.lbl_hide_left.TabIndex = 1;
			this.lbl_hide_left.Click += new System.EventHandler(this.lbl_hide_left_Click);
			this.lbl_hide_left.MouseEnter += new System.EventHandler(this.lbl_hide_left_MouseEnter);
			this.lbl_hide_left.MouseLeave += new System.EventHandler(this.lbl_hide_left_MouseLeave);
			// 
			// txt_show_recv
			// 
			this.txt_show_recv.AcceptsTab = true;
			this.txt_show_recv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_show_recv.BackColor = System.Drawing.SystemColors.Window;
			this.txt_show_recv.ContextMenuStrip = this.menu_show_recv;
			this.txt_show_recv.Location = new System.Drawing.Point(11, 20);
			this.txt_show_recv.Multiline = true;
			this.txt_show_recv.Name = "txt_show_recv";
			this.txt_show_recv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txt_show_recv.Size = new System.Drawing.Size(425, 366);
			this.txt_show_recv.TabIndex = 0;
			this.txt_show_recv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_show_recv_KeyDown);
			this.txt_show_recv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_show_recv_KeyPress);
			// 
			// grp_show_send
			// 
			this.grp_show_send.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.grp_show_send.Controls.Add(this.spc_pack);
			this.grp_show_send.Controls.Add(this.txt_show_send);
			this.grp_show_send.Controls.Add(this.btn_show_send);
			this.grp_show_send.Location = new System.Drawing.Point(0, 0);
			this.grp_show_send.Name = "grp_show_send";
			this.grp_show_send.Size = new System.Drawing.Size(448, 113);
			this.grp_show_send.TabIndex = 0;
			this.grp_show_send.TabStop = false;
			this.grp_show_send.Text = "发送数据";
			// 
			// spc_pack
			// 
			this.spc_pack.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.spc_pack.Location = new System.Drawing.Point(11, 20);
			this.spc_pack.Name = "spc_pack";
			// 
			// spc_pack.Panel1
			// 
			this.spc_pack.Panel1.Controls.Add(this.txt_pack_end);
			this.spc_pack.Panel1.Controls.Add(this.txt_pack_head);
			this.spc_pack.Panel1.Controls.Add(this.chk_pack_end);
			this.spc_pack.Panel1.Controls.Add(this.chk_pack_head);
			this.spc_pack.Panel1MinSize = 204;
			// 
			// spc_pack.Panel2
			// 
			this.spc_pack.Panel2.Controls.Add(this.cbo_pack_check);
			this.spc_pack.Panel2.Controls.Add(this.lbl_pack_interval_ms);
			this.spc_pack.Panel2.Controls.Add(this.chk_pack_check);
			this.spc_pack.Panel2.Controls.Add(this.num_pack_loop);
			this.spc_pack.Panel2.Controls.Add(this.chk_pack_loop);
			this.spc_pack.Panel2MinSize = 215;
			this.spc_pack.Size = new System.Drawing.Size(425, 49);
			this.spc_pack.SplitterDistance = 204;
			this.spc_pack.TabIndex = 13;
			// 
			// txt_pack_end
			// 
			this.txt_pack_end.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_pack_end.Location = new System.Drawing.Point(78, 27);
			this.txt_pack_end.Name = "txt_pack_end";
			this.txt_pack_end.Size = new System.Drawing.Size(125, 21);
			this.txt_pack_end.TabIndex = 3;
			// 
			// txt_pack_head
			// 
			this.txt_pack_head.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_pack_head.Location = new System.Drawing.Point(78, 0);
			this.txt_pack_head.Name = "txt_pack_head";
			this.txt_pack_head.Size = new System.Drawing.Size(125, 21);
			this.txt_pack_head.TabIndex = 1;
			// 
			// chk_pack_end
			// 
			this.chk_pack_end.AutoSize = true;
			this.chk_pack_end.Location = new System.Drawing.Point(0, 29);
			this.chk_pack_end.Name = "chk_pack_end";
			this.chk_pack_end.Size = new System.Drawing.Size(72, 16);
			this.chk_pack_end.TabIndex = 2;
			this.chk_pack_end.Text = "数据帧尾";
			this.chk_pack_end.UseVisualStyleBackColor = true;
			// 
			// chk_pack_head
			// 
			this.chk_pack_head.AutoSize = true;
			this.chk_pack_head.Location = new System.Drawing.Point(0, 2);
			this.chk_pack_head.Name = "chk_pack_head";
			this.chk_pack_head.Size = new System.Drawing.Size(72, 16);
			this.chk_pack_head.TabIndex = 0;
			this.chk_pack_head.Text = "数据帧头";
			this.chk_pack_head.UseVisualStyleBackColor = true;
			// 
			// cbo_pack_check
			// 
			this.cbo_pack_check.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbo_pack_check.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbo_pack_check.FormattingEnabled = true;
			this.cbo_pack_check.Location = new System.Drawing.Point(77, 0);
			this.cbo_pack_check.Name = "cbo_pack_check";
			this.cbo_pack_check.Size = new System.Drawing.Size(140, 20);
			this.cbo_pack_check.TabIndex = 1;
			// 
			// lbl_pack_interval_ms
			// 
			this.lbl_pack_interval_ms.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_pack_interval_ms.AutoSize = true;
			this.lbl_pack_interval_ms.Location = new System.Drawing.Point(188, 31);
			this.lbl_pack_interval_ms.Name = "lbl_pack_interval_ms";
			this.lbl_pack_interval_ms.Size = new System.Drawing.Size(29, 12);
			this.lbl_pack_interval_ms.TabIndex = 4;
			this.lbl_pack_interval_ms.Text = "毫秒";
			// 
			// chk_pack_check
			// 
			this.chk_pack_check.AutoSize = true;
			this.chk_pack_check.Location = new System.Drawing.Point(1, 2);
			this.chk_pack_check.Name = "chk_pack_check";
			this.chk_pack_check.Size = new System.Drawing.Size(72, 16);
			this.chk_pack_check.TabIndex = 0;
			this.chk_pack_check.Text = "数据校验";
			this.chk_pack_check.UseVisualStyleBackColor = true;
			// 
			// num_pack_loop
			// 
			this.num_pack_loop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.num_pack_loop.Location = new System.Drawing.Point(77, 27);
			this.num_pack_loop.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
			this.num_pack_loop.Name = "num_pack_loop";
			this.num_pack_loop.Size = new System.Drawing.Size(105, 21);
			this.num_pack_loop.TabIndex = 3;
			this.num_pack_loop.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			// 
			// chk_pack_loop
			// 
			this.chk_pack_loop.AutoSize = true;
			this.chk_pack_loop.Location = new System.Drawing.Point(1, 29);
			this.chk_pack_loop.Name = "chk_pack_loop";
			this.chk_pack_loop.Size = new System.Drawing.Size(72, 16);
			this.chk_pack_loop.TabIndex = 2;
			this.chk_pack_loop.Text = "周期发送";
			this.chk_pack_loop.UseVisualStyleBackColor = true;
			this.chk_pack_loop.CheckedChanged += new System.EventHandler(this.chk_pack_loop_CheckedChanged);
			// 
			// txt_show_send
			// 
			this.txt_show_send.AcceptsTab = true;
			this.txt_show_send.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_show_send.Location = new System.Drawing.Point(11, 75);
			this.txt_show_send.Multiline = true;
			this.txt_show_send.Name = "txt_show_send";
			this.txt_show_send.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txt_show_send.Size = new System.Drawing.Size(344, 27);
			this.txt_show_send.TabIndex = 0;
			this.txt_show_send.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_show_send_KeyDown);
			// 
			// btn_show_send
			// 
			this.btn_show_send.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_show_send.Location = new System.Drawing.Point(361, 75);
			this.btn_show_send.Name = "btn_show_send";
			this.btn_show_send.Size = new System.Drawing.Size(75, 27);
			this.btn_show_send.TabIndex = 1;
			this.btn_show_send.Text = "发送";
			this.btn_show_send.UseVisualStyleBackColor = true;
			this.btn_show_send.Click += new System.EventHandler(this.btn_show_send_Click);
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(619, 559);
			this.Controls.Add(this.status_bottom);
			this.Controls.Add(this.spc_back);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(635, 597);
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "串口调试助手 - V1.0.3";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.status_bottom.ResumeLayout(false);
			this.status_bottom.PerformLayout();
			this.menu_show_recv.ResumeLayout(false);
			this.spc_back.Panel1.ResumeLayout(false);
			this.spc_back.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spc_back)).EndInit();
			this.spc_back.ResumeLayout(false);
			this.grp_send_config.ResumeLayout(false);
			this.grp_send_config.PerformLayout();
			this.grp_recv_config.ResumeLayout(false);
			this.grp_recv_config.PerformLayout();
			this.grp_port_config.ResumeLayout(false);
			this.grp_port_config.PerformLayout();
			this.spc_show.Panel1.ResumeLayout(false);
			this.spc_show.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spc_show)).EndInit();
			this.spc_show.ResumeLayout(false);
			this.grp_show_recv.ResumeLayout(false);
			this.grp_show_recv.PerformLayout();
			this.grp_show_send.ResumeLayout(false);
			this.grp_show_send.PerformLayout();
			this.spc_pack.Panel1.ResumeLayout(false);
			this.spc_pack.Panel1.PerformLayout();
			this.spc_pack.Panel2.ResumeLayout(false);
			this.spc_pack.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.spc_pack)).EndInit();
			this.spc_pack.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.num_pack_loop)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.StatusStrip status_bottom;
		private System.Windows.Forms.ToolStripStatusLabel slab_recv;
		private System.Windows.Forms.ToolStripStatusLabel slab_info;
		private System.Windows.Forms.ToolStripStatusLabel slab_send;
		private System.Windows.Forms.ContextMenuStrip menu_show_recv;
		private System.Windows.Forms.ToolStripMenuItem CopyToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem PasteToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem SelectAllToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem ClearToolStripMenuItem;
		private System.Windows.Forms.SplitContainer spc_back;
		private System.Windows.Forms.GroupBox grp_send_config;
		private System.Windows.Forms.CheckBox chk_send_lua;
		private System.Windows.Forms.CheckBox chk_send_hex;
		private System.Windows.Forms.CheckBox chk_send_clear;
		private System.Windows.Forms.CheckBox chk_send_file;
		private System.Windows.Forms.Label lbl_send_clear;
		private System.Windows.Forms.Label lbl_send_load;
		private System.Windows.Forms.GroupBox grp_recv_config;
		private System.Windows.Forms.CheckBox chk_send_show;
		private System.Windows.Forms.CheckBox chk_recv_time;
		private System.Windows.Forms.CheckBox chk_recv_file;
		private System.Windows.Forms.Label lbl_recv_clear;
		private System.Windows.Forms.Label lbl_recv_save;
		private System.Windows.Forms.CheckBox chk_recv_show;
		private System.Windows.Forms.CheckBox chk_recv_hex;
		private System.Windows.Forms.GroupBox grp_port_config;
		private System.Windows.Forms.Button btn_port_open;
		private System.Windows.Forms.ComboBox cbo_port_data_bits;
		private System.Windows.Forms.ComboBox cbo_port_stop_bits;
		private System.Windows.Forms.ComboBox cbo_port_parity;
		private System.Windows.Forms.ComboBox cbo_port_baud_rate;
		private System.Windows.Forms.ComboBox cbo_port_name;
		private System.Windows.Forms.Label lbl_port_data_bits;
		private System.Windows.Forms.Label lbl_port_stop_bits;
		private System.Windows.Forms.Label lbl_port_parity;
		private System.Windows.Forms.Label lbl_port_baud_rate;
		private System.Windows.Forms.Label lbl_port_name;
		private System.Windows.Forms.SplitContainer spc_show;
		private System.Windows.Forms.GroupBox grp_show_recv;
		private System.Windows.Forms.Label lbl_hide_down;
		private System.Windows.Forms.Label lbl_hide_left;
		private System.Windows.Forms.TextBox txt_show_recv;
		private System.Windows.Forms.GroupBox grp_show_send;
		private System.Windows.Forms.SplitContainer spc_pack;
		private System.Windows.Forms.TextBox txt_pack_end;
		private System.Windows.Forms.TextBox txt_pack_head;
		private System.Windows.Forms.CheckBox chk_pack_end;
		private System.Windows.Forms.CheckBox chk_pack_head;
		private System.Windows.Forms.ComboBox cbo_pack_check;
		private System.Windows.Forms.Label lbl_pack_interval_ms;
		private System.Windows.Forms.CheckBox chk_pack_check;
		private System.Windows.Forms.NumericUpDown num_pack_loop;
		private System.Windows.Forms.CheckBox chk_pack_loop;
		private System.Windows.Forms.TextBox txt_show_send;
		private System.Windows.Forms.Button btn_show_send;


	}
}

